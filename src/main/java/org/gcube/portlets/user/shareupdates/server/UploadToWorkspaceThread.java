package org.gcube.portlets.user.shareupdates.server;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.UUID;

import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.common.storagehub.client.dsl.FileContainer;
import org.gcube.common.storagehub.client.dsl.FolderContainer;
import org.gcube.common.storagehub.client.dsl.OpenResolver;
import org.gcube.common.storagehub.client.dsl.StorageHubClient;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Massimiliano Assante ISTI-CNR
 *
 */
public class UploadToWorkspaceThread implements Runnable {
	private static Logger _log = LoggerFactory.getLogger(UploadToWorkspaceThread.class);
	public static final String ATTACHMENT_FOLDER ="Shared attachments";
	/**
	 * the identifier of the workspace you are putting
	 */
	private String username;
	/**
	 * the identifier of the workspace you are putting
	 */
	private String fullName;
	/**
	 * the name of the file you are putting
	 */
	private String fileName;
	/**
	 * the path (with name) of the file you are putting
	 */
	private String fileabsolutePathOnServer;

	/**
	 * 
	 * @param sClient the instance of the storage client
	 * @param fileToUpload the absolute path of the file
	 */
	public UploadToWorkspaceThread(String fullName, String username, String fileName, String fileabsolutePathOnServer) {
		super();
		this.username = username;
		this.fullName = fullName;
		this.fileName = fileName;
		this.fileabsolutePathOnServer = fileabsolutePathOnServer;
	}

	@Override
	public void run() {
		try {
			String currContext = ScopeProvider.instance.get();
			StorageHubClient shc = new StorageHubClient();
			_log.debug("Trying to get VRE folder for scope="+currContext);
			FolderContainer vreFolder = shc.openVREFolder();

			_log.debug("File to upload="+fileabsolutePathOnServer);
			File file = new File(fileabsolutePathOnServer);
			InputStream fileData = new FileInputStream(file);
			FolderContainer attachmentFolder = null;
			try { 
				OpenResolver oRes = vreFolder.openByRelativePath(ATTACHMENT_FOLDER);
				attachmentFolder = oRes.asFolder();
			} catch (StorageHubException e) {
				_log.info(ATTACHMENT_FOLDER + " in VRE Folder does not exists, creating it for "+currContext);
				attachmentFolder = vreFolder.newFolder(ATTACHMENT_FOLDER,"Folder created automatically by the System");
			}

			String itemName = getUniqueName(fileName);
			FileContainer uploadedFile = attachmentFolder.uploadFile(fileData, itemName, "File shared by " + fullName + "("+username+")");
			_log.info("Uploaded " + uploadedFile.get().getName() + " - Returned Workspace id=" + uploadedFile.getId());	
		}
		catch (Exception e) {			
			e.printStackTrace();
			_log.error("Something wrong while uploading " + fileName + " in Workspace VRE Folder " + ATTACHMENT_FOLDER + ": "+ e.getMessage());
		}
	}

	private String getUniqueName(String filename) {
		return UUID.randomUUID().toString().substring(0, 8) + "_" + filename;
	}
}
