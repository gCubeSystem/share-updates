# Changelog for Share-Updates Portlet

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v3.0.0]

- removed cassandra 2 client
- maven-parent 1.2.0
- maven-portal-bom 4.0.0
- StorageHub downstream components to upgrade in order to work with storagehub 1.5.0 [#27999]

## [v2.9.0] - 2023-07-21

- enlarge image preview in posts

## [v2.8.1] -2022-06-15

- Fixed soem deps

## [v2.8.0] -2021-04-02

- Ported to git

- Fix bug attached documents in Posts do not open in Chrome [#21023]

- Remove HomeLibrary dependency and replace with storage hub one

## [v2.7.1] -2020-04-28

- Feature #18992 Reconsider the alert message raised when writing a post containing more than one link

## [v2.7.0] - 2019-03-29

- Revised social networking library mechanism for http links, mentions and hashtags recognition [#13207]

## [v2.6.2] - 2019-01-04

- Fixed incident in some VRE may not work [#13096]

## [v2.6.1] - 2018-04-12

- Removed previous jquery js load script

## [v2.6.0] - 2018-02-07

- restyled checkbox for notifications
- Removed deprecated TIBCO Pagebus library and replaces with Liferay's IPC Client Side
- Enhanced efficiency when retrieving mentioned users or groups in posts

## [v2.5.0] - 2017-12-07

- drop down menu replaced with checkbox for notifications [#9971]

## [v2.3.1] - 2017-04-12

- Ported to Java 8
- Fix [#7630]

## [v2.3.0] - 2017-01-20

- Updated method for using ftp server
- Minor fix when VREs' teams are notified

## [v2.1.0] - 2016-11-29

- updated opengraph checkURL for getting images in case of HTTP redirect permanently is encountered

## [v2.0.0] - 2016-06-29

- Updated to Liferay 6.2.5

## [v1.9.0] - 2016-01-22

- Multi-attachment upload is now supported
- Drag and drop mechanism added for attachments
- Restyled to optimize vertical space

## [v1.8.0] - 2015-07-06

- made notification to members option configurable as default [#121]

## [v1.7.0] - 2015-05-27

- Integrated gwt-bootstrap and revised css
- ported to GWT 2.7.0

## [v1.6.0] - 2014-11-03

- Added Client scope handler, to set the scope from the client and help preventing the browser back button cache problem
- Added possibility to define topics in a post using hashtags
- Added save attachments in VRE Group folder

## [v1.3.0] - 2014-05-09

- Added possibility to alert the users of a VRE when sharing an update
- Refined the way to guess content and images when parsing HTML
- Improved images recognition when parsing html pages having no image indication from openGraph
- Added user agent property to http requests to avoid web servers 'complaining' HTTP 403 errors
- Replaced all the line breaks and all the double spaces with the HTML version to preserve new lines in posts
- Fixed bug when changing user avatar and minor restyle
- Several improvements in url checkings
- resolved problem with AdBlock plugin
- Fixed mention user dialog y-position when textbox is higher than usual due to new lines
- Mention users list now returns only the list of the available users in the current organization
- Fixed posts when multiple tabs are open, now the status is kept on the client

## [v1.1.3] - 2014-02-10

- added thumbnail generation and upload on FTP server
- added upload a copy on my workspace file when sharing a file
- implemented the file preview with icons or preview depending on the extension of the file
- integrated file uplod progress bar widget for sharing files
- fixed bug allowing users to post while generating file/url previews
- fixed bug allowing users to mention theirselves

## [v1.1.0] - 2013-10-21

- Ported to GWT 251
- Ported to Feather Weight Stack
- Removed GCF Dependency
- Logging with sl4j Enabled
- Fixed image previes retrieval when image is in the same folder of the linked page

## [v1.0.0] - 2013-01-13

- First release
